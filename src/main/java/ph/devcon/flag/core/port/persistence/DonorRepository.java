package ph.devcon.flag.core.port.persistence;

import java.util.List;

import ph.devcon.flag.core.component.donor.domain.Donor;

public interface DonorRepository {
    List<Donor> findAllDonors();
    void createUpdateDonor(Donor donor);
    Donor findById(int id);
    Donor findByMobile(String mobile);
    List<Donor> findByType(int donorTypeId);
    List<Donor> findBySector(int sectorTypeId);
}
