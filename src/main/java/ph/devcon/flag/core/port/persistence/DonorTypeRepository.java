package ph.devcon.flag.core.port.persistence;

import java.util.List;

import ph.devcon.flag.core.component.donor.domain.DonorType;

public interface DonorTypeRepository {
    List<DonorType> findAllDonorTypes();
    DonorType findByName(String donorType);
    DonorType findById(int id);
}

