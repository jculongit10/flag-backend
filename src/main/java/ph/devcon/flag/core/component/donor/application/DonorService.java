package ph.devcon.flag.core.component.donor.application;

import java.util.List;

import ph.devcon.flag.core.component.donor.domain.Donor;
import ph.devcon.flag.entrypoint.api.rest.donor.DonorDTO;

public interface DonorService {
    void createUpdateDonor(DonorDTO donor) throws Exception;
    List<Donor> getDonors();
    List<Donor> getDonorsBySector(int sectorId);
    List<Donor> getDonorsByType(int typeId);
    Donor getDonor(int id);
    Donor getDonorByMobile(String mobileNumber);
}
