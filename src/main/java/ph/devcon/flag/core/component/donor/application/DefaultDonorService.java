package ph.devcon.flag.core.component.donor.application;

import ph.devcon.flag.core.component.donor.domain.Donor;
import ph.devcon.flag.core.component.donor.domain.DonorType;
import ph.devcon.flag.core.component.donor.domain.SectorType;
import ph.devcon.flag.core.component.exception.InvalidRequestException;
import ph.devcon.flag.core.port.persistence.DonorRepository;
import ph.devcon.flag.core.port.persistence.DonorSectorRepository;
import ph.devcon.flag.core.port.persistence.DonorTypeRepository;
import ph.devcon.flag.entrypoint.api.rest.donor.DonorDTO;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class DefaultDonorService implements DonorService {

    @Inject
    DonorRepository donorRepository;

    @Inject
    DonorSectorRepository sectorRepository;

    @Inject
    DonorTypeRepository donorTypeRepository;
    
    @Override
    public Donor getDonor(final int id) {
        return donorRepository.findById(id);
    }

    @Override
    public List<Donor> getDonors() {
        return donorRepository.findAllDonors();
    }

    @Override
    public List<Donor> getDonorsBySector(int sectorId) {
       return donorRepository.findBySector(sectorId);
    }

    @Override
    public List<Donor> getDonorsByType(int typeId) {
        return donorRepository.findByType(typeId);
    }

    @Override
    public Donor getDonorByMobile(String mobileNumber) {
        return donorRepository.findByMobile(mobileNumber);
    }

    @Transactional
    @Override
    public void createUpdateDonor(DonorDTO donorDTO) throws Exception {
        
        String affiliation = donorDTO.affiliation;
        String name = donorDTO.name;
        
        if(affiliation == null || affiliation.isEmpty() && name == null || name.isEmpty()) 
            throw new InvalidRequestException("Donor must have at least a name or affiliation.");

        SectorType sector = sectorRepository.findByName(donorDTO.sectorType);
        if(sector == null)
            throw new InvalidRequestException("Invalid donor sector.");

        DonorType type = donorTypeRepository.findByName(donorDTO.donorType);
        if(type == null)
            throw new InvalidRequestException("Invalid donor type.");

        Donor donor = new Donor();
        donor.setName(name);
        donor.setAffiliation(affiliation);
        donor.setSectorType(sector);
        donor.setDonorType(type);
        donor.setEmail(donorDTO.email);
        donor.setMobileNumber(donorDTO.mobileNumber);
        donorRepository.createUpdateDonor(donor);
    }
}
