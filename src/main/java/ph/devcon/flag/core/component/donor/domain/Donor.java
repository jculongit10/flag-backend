package ph.devcon.flag.core.component.donor.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import static javax.persistence.FetchType.EAGER;

@Data
@Entity(name = "donors")
public class Donor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "affiliation_org")
    private String affiliation;

    @Column(name = "representative_name")
    private String name;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @ManyToOne(targetEntity = DonorType.class, fetch = EAGER)
    @JoinColumn(name = "donor_type_id")
    private DonorType donorType;

    @ManyToOne(targetEntity = SectorType.class, fetch = EAGER)
    @JoinColumn(name = "sector_type_id")
    private SectorType sectorType;

    @Column(name = "email")
    private String email;

    public Donor() {
    }
}
