package ph.devcon.flag.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ph.devcon.flag.core.component.donor.domain.Donor;
import ph.devcon.flag.core.port.persistence.DonorRepository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PanacheDonorRepository implements DonorRepository, PanacheRepository<Donor> {


    @Override
    public List<Donor> findAllDonors() {
        return findAll().list();
    }

    @Override
    public Donor findById(final int id) {
        return find("id", id).firstResult();
    }

    @Override
    public Donor findByMobile(final String mobile) {
        return find("mobile_number", mobile).firstResult();
    }

    @Override
    public List<Donor> findByType(int donorTypeId) {
        return find("donor_type_id", donorTypeId).list();
    }

    @Override
    public List<Donor> findBySector(int sectorTypeId) {
        return find("sector_type_id", sectorTypeId).list();
    }

    @Override
    public void createUpdateDonor(Donor donor) {
        persistAndFlush(donor);
    }

}
