package ph.devcon.flag.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ph.devcon.flag.core.component.donor.domain.DonorType;
import ph.devcon.flag.core.port.persistence.DonorTypeRepository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;;

@ApplicationScoped
public class PanacheDonorTypeRepository implements DonorTypeRepository, PanacheRepository<DonorType> {

    @Override
    public List<DonorType> findAllDonorTypes() {
        return findAll().list();
    }

    @Override
    public DonorType findByName(String name) {
        return find("name", name).firstResult();
    }

    @Override
    public DonorType findById(int id) {
        return find("id", id).firstResult();
    }
}
