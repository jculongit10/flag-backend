package ph.devcon.flag.entrypoint.api.rest.donor;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DonorDTO {
    
    @JsonProperty("id")
    public int id;

    @JsonProperty("affiliation_org")
    public String affiliation;

    @JsonProperty("representative_name")
    public String name;

    @JsonProperty("mobile_number")
    public String mobileNumber;

    @JsonProperty("donor_type")
    public String donorType;

    @JsonProperty("sector_type")
    public String sectorType;

    @JsonProperty("email")
    public String email;
}