package ph.devcon.flag.entrypoint.api.rest.donor;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The response to the get donor endpoint. This gets serialized into JSON.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetDonorResponse {

    @JsonProperty("id")
    private int id;

    @JsonProperty("affiliation")
    private String affiliation;

    @JsonProperty("name")
    private String name;

    @JsonProperty("mobile_number")
    private String mobileNumber;

    @JsonProperty("donor_type")
    private String donorType;

    @JsonProperty("sector_type")
    private String sectorType;

    @JsonProperty("email")
    private String email;
}
