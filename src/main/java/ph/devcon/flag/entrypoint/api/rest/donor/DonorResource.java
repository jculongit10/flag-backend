package ph.devcon.flag.entrypoint.api.rest.donor;

import lombok.extern.slf4j.Slf4j;
import ph.devcon.flag.core.component.donor.application.DonorService;
import ph.devcon.flag.core.component.donor.domain.Donor;
import ph.devcon.flag.core.component.exception.InvalidRequestException;
import ph.devcon.flag.entrypoint.api.rest.error.JsonError;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.jboss.resteasy.spi.HttpResponseCodes.SC_INTERNAL_SERVER_ERROR;
import static org.jboss.resteasy.spi.HttpResponseCodes.SC_NOT_FOUND;
import static org.jboss.resteasy.spi.HttpResponseCodes.SC_BAD_REQUEST;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Path("/donors")
@Produces(APPLICATION_JSON)
@ApplicationScoped
public class DonorResource {

    @Inject
    DonorService donorService;

    @POST
    @Path("/")
    public Response createDonor(DonorDTO donor) throws FileNotFoundException {
        Response response  = Response.ok().build();
        try {
            donorService.createUpdateDonor(donor);
        }catch(InvalidRequestException e){
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("400")
                    .title("Invalid Request")
                    .detail("TODO: FIX THIS")
                    .build();
            response = Response.status(SC_BAD_REQUEST).entity(error).build();
        } 
        catch (Exception e) {
            PrintStream s = new PrintStream("debug.txt");
            e.printStackTrace(s);
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail(e.getMessage())
                    .build();

            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return response;
    }

    @GET
    @Path("/")
    public Response getDonors() {
        Response response;
        try {
            List<Donor> donors = donorService.getDonors();
            response = buildDonorResponseList(donors);

        } catch (Exception e) {
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail("TODO: FIX THIS")
                    .build();

            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return response;
    }

    @GET
    @Path("/{id}")
    public Response getDonor(@PathParam("id") final int id) {
        Response response;
        try {
            Donor donor = donorService.getDonor(id);
            response = buildDonorResponse(donor);

        } catch (Exception e) {
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail("TODO: FIX THIS")
                    .build();
            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }
        return response;
    }

    @GET
    @Path("/mobile/{number}")
    public Response getDonorByMobile(@PathParam("number") String number) {
        Response response;
        try {

            Donor donor = donorService.getDonorByMobile(number);
            response = buildDonorResponse(donor);

        } catch (Exception e) {
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail("TODO: FIX THIS")
                    .build();

            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return response;
    }

    @GET
    @Path("/type/{id}")
    public Response getDonorByType(@PathParam("id") int donorTypeId) {
        Response response;
        try {
            List<Donor> donors = donorService.getDonorsByType(donorTypeId);
            response = buildDonorResponseList(donors);

        } catch (Exception e) {
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail("TODO: FIX THIS")
                    .build();

            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return response;
    }

    
    @GET
    @Path("/sector/{id}")
    public Response getDonorBySector(@PathParam("id") int sectorId) {
        Response response;
        try {
            List<Donor> donors = donorService.getDonorsBySector(sectorId);
            response = buildDonorResponseList(donors);

        } catch (Exception e) {
            log.error("error occurred querying by id", e);
            JsonError error = JsonError.builder()
                    .status("500")
                    .title("An Unknown Error has occurred")
                    .detail("TODO: FIX THIS")
                    .build();

            response = Response.status(SC_INTERNAL_SERVER_ERROR).entity(error).build();
        }

        return response;
    }


    private Response buildDonorResponseList(List<Donor> donors){
        List<DonorDTO> retval = new ArrayList<DonorDTO>();
        for(Donor donor:donors){          
            if (donor != null) {
                DonorDTO donorResponse = DonorDTO.builder()
                        .id(donor.getId())
                        .affiliation(donor.getAffiliation())
                        .name(donor.getName())
                        .mobileNumber(donor.getMobileNumber())
                        .donorType(donor.getDonorType().getName())
                        .sectorType(donor.getSectorType().getName())
                        .email(donor.getEmail())
                        .build();

                retval.add(donorResponse);
            } else {
                JsonError error = JsonError.builder()
                        .status("404")
                        .title("Donor Not Found")
                        .detail("The donor selected was not found")
                        .build();

                return Response.status(SC_NOT_FOUND).entity(error).build();
            }    
        }

        return Response.ok(retval).build();
    }

    private Response buildDonorResponse(Donor donor) {
        Response response;
        if (donor != null) {
            DonorDTO requestorResponse = DonorDTO.builder()
                    .id(donor.getId())
                    .affiliation(donor.getAffiliation())
                    .name(donor.getName())
                    .mobileNumber(donor.getMobileNumber())
                    .donorType(donor.getDonorType().getName())
                    .sectorType(donor.getSectorType().getName())
                    .email(donor.getEmail())
                    .build();
           response = Response.ok(requestorResponse).build();
        } else {
            JsonError error = JsonError.builder()
                    .status("404")
                    .title("Donor Not Found")
                    .detail("The donor selected was not found")
                    .build();
            response = Response.status(SC_NOT_FOUND).entity(error).build();
        }

        return response;
    }
}
