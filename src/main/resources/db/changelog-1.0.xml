<?xml version="1.0" encoding="UTF-8"?>

<databaseChangeLog
        xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
         http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.8.xsd">

    <preConditions>
        <dbms type="postgresql"/>
    </preConditions>
      
    <changeSet id="create-address-table" author="nitz">
        <createTable tableName="address">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="facility_name" type="varchar(50)"/>
            <column name="contact_name" type="varchar(50)"/>
            <column name="mobile_number" type="varchar(20)"/>
            <column name="email" type="varchar(20)"/>
            <column name="address_line_one" type="varchar(50)"/>
            <column name="barangay" type="varchar(50)"/>
            <column name="city" type="varchar(50)">
                <constraints nullable="false"/>
            </column>
            <column name="province" type="varchar(50)">
                <constraints nullable="false"/>
            </column>
            <column name="landmark" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="create-requestor-types-table" author="dexter">
        <createTable tableName="requestor_types">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-requestor-types" author="dexter">
        <loadData tableName="requestor_types"
                  file="data/requestor-types-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-requestor-statuses-table" author="dexter">
        <createTable tableName="requestor_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-requestor-statuses" author="dexter">
        <loadData tableName="requestor_statuses"
                  file="data/requestor-status-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>
    
    <changeSet id="create-requestors-table" author="dexter">
        <createTable tableName="requestors">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="first_name" type="varchar(50)"/>
            <column name="last_name" type="varchar(50)"/>
            <column name="mobile_number" type="varchar(50)"/>
            <column name="requestor_type_id" type="int">
                <constraints foreignKeyName="fk_requestor_type"
                             referencedTableName="requestor_types"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="group" type="varchar(50)"/>
            <column name="email" type="varchar(50)"/>
            <column name="requestor_status_id" type="int">
                <constraints foreignKeyName="fk_requestor_status"
                             referencedTableName="requestor_statuses"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create-sector-types-table" author="nitz">
        <createTable tableName="sector_types">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-sector-types" author="dexter">
        <loadData tableName="sector_types"
                  file="data/sector-types-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-donor-types-table" author="dexter">
        <createTable tableName="donor_types">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-donor-types" author="dexter">
        <loadData tableName="donor_types"
                  file="data/donor-types-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-donor-statuses-table" author="dexter">
        <createTable tableName="donor_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-donor-statuses" author="dexter">
        <loadData tableName="donor_statuses"
                  file="data/donor-statuses-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-donors-table" author="dexter">
        <createTable tableName="donors">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="affiliation_org" type="varchar(50)"/>
            <column name="representative_name" type="varchar(50)"/>
            <column name="mobile_number" type="varchar(20)"/>
            <column name="donor_type_id" type="int">
                <constraints foreignKeyName="fk_donor_type"
                             referencedTableName="donor_types"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="sector_type_id" type="int">
                <constraints foreignKeyName="fk_donor_sector"
                             referencedTableName="sector_types"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="address_id" type="int">
                <constraints foreignKeyName="fk_donor_address"
                             referencedTableName="address"
                             referencedColumnNames="id"
                             nullable="true" />
            </column>
            <column name="email" type="varchar(50)"/>
        </createTable>
    </changeSet>
    
    <changeSet id="create-request-types-table" author="dexter">
        <createTable tableName="request_types">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-request-types" author="dexter">
        <loadData tableName="request_types"
                  file="data/request-types-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-request-statuses-table" author="nitz">
        <createTable tableName="request_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
            <column name="order" type="int"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-request-statuses" author="nitz">
        <loadData tableName="request_statuses"
                  file="data/request-statuses-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-signoff-statuses-table" author="nitz">
        <createTable tableName="signoff_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
            <column name="order" type="int"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-signoff-statuses" author="nitz">
        <loadData tableName="signoff_statuses"
                  file="data/signoff-statuses-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>
    
    <changeSet id="create-currencies-table" author="nitz">
        <createTable tableName="currencies">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="code" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-currencies" author="nitz">
        <loadData tableName="currencies"
                  file="data/currencies-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-donation-requests-table" author="dexter">
        <createTable tableName="donation_requests">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="donor_id" type="int">
                <constraints foreignKeyName="fk_donotion_requests_donor"
                             referencedTableName="donors"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="request_type_id" type="int">
                <constraints foreignKeyName="fk_donation_requests_request_type"
                             referencedTableName="request_types"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="request_status_id" type="int">
                <constraints foreignKeyName="fk_donation_request_status"
                             referencedTableName="request_statuses"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="signoff_status_id" type="int">
                <constraints foreignKeyName="fk_donation_request_signoff_status"
                             referencedTableName="signoff_statuses"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="currency_id" type="int">
                <constraints foreignKeyName="fk_donation_currency"
                             referencedTableName="currencies"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="airtable_id" type="text"/>
            <column name="estimatedvalue" type="double"/>
            <column name="createdate" type="TIMESTAMP without time zone"/>
            <column name="lastmodified" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

    <changeSet id="create-packages-table" author="dexter">
        <createTable tableName="packages">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="donor_request_id" type="int">
                <constraints foreignKeyName="fk_package_donation_request"
                             referencedTableName="donation_requests"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="package_remarks" type="text"/>
            <column name="currency_id" type="int">
                <constraints foreignKeyName="fk_package_currency"
                             referencedTableName="currencies"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="total_weight" type="double"/>
            <column name="value" type="double"/>
        </createTable>
    </changeSet>

    <changeSet id="create-item-categories-table" author="dexter">
        <createTable tableName="item_categories">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-item-categories" author="dexter">
        <loadData tableName="item_categories"
                  file="data/item-categories-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-item-units-table" author="dexter">
        <createTable tableName="units">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="code" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-units" author="dexter">
        <loadData tableName="units"
                  file="data/units-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-package-contents-table" author="dexter">
        <createTable tableName="package_contents">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="item_category_id" type="int">
                <constraints foreignKeyName="fk_package_content_item_category"
                             referencedTableName="item_categories"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="weight" type="double"/>
            <column name="unit_id" type="int">
                <constraints foreignKeyName="fk_package_content_unit"
                             referencedTableName="units"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="total_unit_value" type="double">
                <constraints nullable="false"/>
            </column>
            <column name="remaining_unit_value" type="double"/>
            <column name="currency_id" type="int">
                <constraints foreignKeyName="fk_package_content_currency"
                             referencedTableName="currencies"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="monetary_value" type="double"/>
            <column name="item_description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="create-dispatch-statuses-table" author="nitz">
        <createTable tableName="dispatch_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(50)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
            <column name="order" type="int"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-dispatch-statuses" author="nitz">
        <loadData tableName="dispatch_statuses"
                  file="data/dispatch-statuses-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-pickup-statuses-table" author="nitz">
        <createTable tableName="pickup_statuses">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(50)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="description" type="varchar(50)"/>
            <column name="order" type="int"/>
        </createTable>
    </changeSet>

    <changeSet id="load-initial-pickup-statuses" author="nitz">
        <loadData tableName="pickup_statuses"
                  file="data/pickup-statuses-1.0.csv"
                  relativeToChangelogFile="true"
                  quotchar="'"
                  separator=","
                  usePreparedStatements="true"/>
    </changeSet>

    <changeSet id="create-fleets-table" author="nitz">
        <createTable tableName="fleets">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(20)">
                <constraints unique="true" nullable="false"/>
            </column>
            <column name="fleet_contact" type="varchar(50)"/>
            <column name="fleet_leader" type="varchar(50)"/>
            <column name="description" type="varchar(50)"/>
        </createTable>
    </changeSet>

    <changeSet id="create-inbound-dispatches-table" author="nitz">
        <createTable tableName="inbound_dispatches">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="pickup_location_id" type="int">
                <constraints foreignKeyName="fk_inbound_pickup_location"
                             referencedTableName="address"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="package_id" type="int">
                <constraints foreignKeyName="fk_inbound_package"
                             referencedTableName="packages"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="fleet_id" type="int">
                <constraints foreignKeyName="fk_inbound_fleet"
                             referencedTableName="fleets"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="request_remark" type="varchar(50)"/>
            <column name="photo_reference" type="varchar(50)"/>
            <column name="pickup_timestamp" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

    <changeSet id="create-pickup-status-history-table" author="nitz">
        <createTable tableName="pickup_status_history">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="inbound_id" type="int">
                <constraints foreignKeyName="fk_current_pickup_status_inbound"
                             referencedTableName="inbound_dispatches"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="status_id" type="int">
                <constraints foreignKeyName="fk_current_pickup_status"
                             referencedTableName="pickup_statuses"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="date" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

    <changeSet id="create-outbound-dispatches-table" author="nitz">
        <createTable tableName="outbound_dispatches">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>

            <column name="destination_id" type="int">
                <constraints foreignKeyName="fk_outbound_destination"
                             referencedTableName="address"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>

            <column name="package_id" type="int">
                <constraints foreignKeyName="fk_outbound_package"
                             referencedTableName="packages"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="fleet_id" type="int">
                <constraints foreignKeyName="fk_outbound_fleet"
                             referencedTableName="fleets"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="unit_id" type="int">
                <constraints foreignKeyName="fk_outbound_dispatch_unit"
                             referencedTableName="units"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="total_unit_value" type="double">
                <constraints nullable="false"/>
            </column>
            <column name="photo_reference" type="varchar(50)"/>
            <column name="delivery_remark" type="varchar(50)"/>
            <column name="pickup_timestamp" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>


    <changeSet id="create-dispatch-status-history-table" author="nitz">
        <createTable tableName="dispatch_status_history">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="inbound_id" type="int">
                <constraints foreignKeyName="fk_current_dispatch_status_inbound"
                             referencedTableName="outbound_dispatches"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="status_id" type="int">
                <constraints foreignKeyName="fk_current_dispatch_status"
                             referencedTableName="dispatch_statuses"
                             referencedColumnNames="id"
                             nullable="false" />
            </column>
            <column name="date" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

    <changeSet id="create-schedules-table" author="dexter">
        <createTable tableName="schedules">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="description" type="text"/>
            <column name="schedule_start" type="TIMESTAMP without time zone"/>
            <column name="schedule_end" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

    <changeSet id="create-pickup-schedules-table" author="dexter">
        <createTable tableName="pickup_schedules">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="pickup_id" type="int">
                <constraints foreignKeyName="fk_pickup_schedules_inbound"
                             referencedTableName="inbound_dispatches"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="schedule_id" type="int">
                <constraints foreignKeyName="fk_pickup_schedules_schedule"
                             referencedTableName="schedules"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create-delivery-schedules-table" author="dexter">
        <createTable tableName="delivery_schedules">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="dispatch_id" type="int">
                <constraints foreignKeyName="fk_delivery_schedules_outbound"
                             referencedTableName="outbound_dispatches"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
            <column name="schedule_id" type="int">
                <constraints foreignKeyName="fk_pickup_schedules_schedule"
                             referencedTableName="schedules"
                             referencedColumnNames="id"
                             nullable="false"/>
            </column>
        </createTable>
    </changeSet>

    <changeSet id="create-otp-requests-table" author="dexter">
        <createTable tableName="otp_requests">
            <column name="id" type="int" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="mobile_number" type="varchar(20)">
                <constraints unique="true"/>
            </column>
            <column name="transaction_type" type="varchar(20)"/>
            <column name="reference_code" type="varchar(20)"/>
            <column name="validation_code" type="varchar(20)"/>
            <column name="expiry" type="TIMESTAMP without time zone"/>
        </createTable>
    </changeSet>

</databaseChangeLog>