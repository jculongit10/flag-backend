1.)packages
   a.)packages
   b.)packages/{id}
   c.)packages/{status}
   d.)packages/{category}
   e.)packages/{type} (received, pickup)
   f.)packages/{date}
   g.)packages/from/{startedate}/to/{enddate}
   h.)packages/{dispatch_status}
   i.)packagecontents/{package_id}

2.)donors
   a.)donors
   b.)donors/{id}
   c.)donors/{type}
   d.)donors/{sector} 

3.)donations (aggregated data like that in the Airtable. Can be used for reporting)
   a.)donations
   b.)donations/{id}
   c.)donations/{donor_id}
   d.)donations/{date}
   e.)donations/from/{startdate}
   f.)donations/from/{startdate}/to/{enddate}

4.)pickups
   a.)pickups
   b.)pickups/{id}
   c.)pickups/{package_id}
   d.)pickups/{date}
   e.)pickups/{donations_status}
   f.)pickups/{fleet_id}

4.)dispatches
   a.)dispatches
   b.)dispatches
   c.)dispatches/{package_id}
   d.)dispatches/{date}
   e.)dispatches/{donations_status}
   f.)dispatches/{fleet_id}

5.)fleets
   a.)fleets
   b.)fleets{id}

6.)dispatch_statuses
   a.)dispatchstatuses
   b.)dispatchstatuses/{id}

7.)donation_status
   a.)donationstatuses
   b.)donationstatuses/{id}

8.)pickup_schedules
   a.)pickupschedules
   b.)pickupschedules/{id}
   c.)pickupschedules/{date}
   c.)pickupschedules/from/{startdate}/to/{enddate}

9.)item_categories
   a.)itemcategories
   b.)itemcategories/{id}

10.)donor_types
   a.)donor_types
   b.)donor_types/{id}

11.)sector_types
   a.)sectortypes
   b.)sectortypes{id}

12.)otp_requests
   a.)otp_requests
   b.)otp_requests/{id}
   c.)otp_requests/{mobilenumber}

13.)received_items
   a.)receiveditems
   b.)receiveditems/{id}
   c.)receiveditems{date}
   