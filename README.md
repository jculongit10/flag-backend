# DCTx FLAG Backend

The DCTx FLAG Backend is the application supporting the Mobile and the
Dashboard apps.

The projects uses Quarkus and follows the Explicit Architecture as a guideline. For details:

- [Quarkus](https://quarkus.io)
- [Explicit Architectrure](https://herbertograca.com/tag/explicit-architecture/)

The API is documented using OpenAPI v3.x:

- [OpenAPI Spec](src/main/resources/META-INF/openapi.yaml).

## Documents

The [WIKI](https://gitlab.com/dctx/flag/flag-user-stories/-/wikis) will contain all official documents of this project.

TODO: Add links for other resources for now

## Getting Started

### Prerequisite

- JDK 9+
- Maven 3.6.2+ (or use the maven wrapper included)
- Docker
- Docker Compose
- kubectl
- helm

### Cloning the repository

```
$ git clone git@gitlab.com/dctx/flag/flag-backend.git
```

### Build

```
$ ./mvnw quarkus:dev
```

### Configuration

Configuration is via the `application.properties` file. These can be overridden
using environment.

// TODO: Add env vars table here

### Starting the Services

The application depends on different external applications and are conveniently
added in a docker-compose file. To spin up the dependencies:

```
$ docker-compose up -d
```

The containers can be stopped using:

```
$ docker-compose stop
```

Or completely removed using:

```
$ docker-compose down
```

Note that volumes are not configured yet in the docker-compose spec and bringing
down the containers will remove all data.

#### Keycloak

Keycloak is the IAM used by this application. On service start, the Keycloak
instance will not have a realm configured yet.

The [flag-realm.json](docker-files/flag-realm.json) should be imported by
creating a new realm inside keycloak's user interface.

Keycloak is accessible via: [http://localhost:8082/auth](http://localhost:8082/auth)

Default credentials are:

```
username: keycloak
password: keycloak
```

#### Postgres

A postgres database is included in the docker-compose spec. This postgres
instance contains two databases with the following credentials:

Keycloak DB:

```
host: localhost
port: 5432
database: keycloak
username: keycloak
password: keycloak
```

FLAG DB:

```
host: localhost
port: 5432
database: flag
username: flag
password: flag
```

#### Kafka and Zookeper

Kafka will be used to perform asynchronous notifications. It depends on
Zookeeper, which acts as a centralized service to provide synchronization
within distributed systems.

Kafka:

```
ALLOW_PLAINTEXT_LISTENER = yes
KAFKA_CFG_ZOOKEEPER_CONNECT: zookeeper = 2181
KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP = PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
KAFKA_CFG_LISTENERS = PLAINTEXT://:9092,PLAINTEXT_HOST://:29092
KAFKA_CFG_ADVERTISED_LISTENERS = PLAINTEXT://kafka:9092,PLAINTEXT_HOST://localhost:29092
```

- `ALLOW_PLAINTEXT_LISTENER`: Allow to use the PLAINTEXT listener
- `KAFKA_CFG_ZOOKEEPER_CONNECT`: Comma separated host:port pairs,
  each corresponding to a Zookeeper Server
- `KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP`: Maps the security protocol
  of the listeners
- `KAFKA_CFG_LISTENERS`: Interfaces that Kafka binds to
- `KAFKA_CFG_ADVERTISED_LISTENERS`: How clients can connect

Zookeeper:

```
ALLOW_ANONYMOUS_LOGIN = yes
```

- `ALLOW_ANONYMOUS_LOGIN`: Allows anonymous users to connect

#### MinIO

For object storage, we will be using MinIO to easily plug other cloud object
storage systems such as Amazon S3, Azure Object Storage, etc.

To override MinIO's auto-generated keys, you may pass secret and access keys
explicitly as environment variables. MinIO server also allows regular strings as access and secret keys:

```
MINIO_ACCESS_KEY = minio
MINIO_SECRET_KEY = minio123
```

A directory gets created in the container filesystem at the time of container start
but all the data is lost after container exits. To create a MinIO container with persistent storage,
uncomment the following in the configuration:

```
- ./docker-files/minio/data:/data
```

## Development

Please check [CONTRIBUTING.md](CONTRIBUTING.md) first.

### Code Quality
Check if your code adheres to the project's coding standard:
```
$ ./mvnw checkstyle:check
$ ./mvnw spotbugs:spotbugs
$ ./mvnw verify
```

## Deployment

// TODO: add deployment details here

### Packaging

### Installation
